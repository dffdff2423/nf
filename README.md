# NF programming language

A work in progress programming language. Intended to function somewhat like c
but with a more modern syntax & utilities, like defer and generics (see doc/ex.nf).
Currently the compiler only supports function calls and arithmetic and uses
LLVM as a backend. I got somewhat burned out with this project so it will likely
not be updated for a while.

## Compiling

Building the compiler and runtime:

```sh
cargo build
./rt/buildobj.sh
```

Building programs:
```sh
./target/debug/nfc file.nf -o file.o
./link.sh file.o
# run program:
./nf-prog
```
