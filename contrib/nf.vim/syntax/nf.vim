" nf syntax
" Language: nf

if exists("b:current_syntax")
    finish
endif

syn keyword nfKeyword nt nf imp ni prove return glbl glblm module
syn keyword nfConditional if else match
syn keyword nfRepeat loop break
syn match nfOperator display ":=\|?\|:\|\$"
syn keyword nfNewtype struct func errtype
syn keyword nfBool true false
" FIXME
hi def link nfBool nfNewtype
" FIXME
syn match nfNamsepacePath "\w*::"he=e-3,me=e-3

syn match nfNumber display "\<[0-9]*"

syn region nfComment start="//" end="$"
syn region nfComment start="/\*" end="\*/"
syn region nfString start="\"" end="\""

hi def link nfKeyword Keyword
hi def link nfConditional Conditional
hi def link nfRepeat Repeat
hi def link nfOperator Operator
hi def link nfNewtype Structure
hi def link nfNamespacePath Include
hi def link nfComment Comment
hi def link nfNumber Number
hi def link nfString String

let b:current_syntax = "nf"
