// at this stage there is probably syntax errors in this file

module example;

// importing, I want to encorage flat namespaces
imp std::*;

// nt -- new type
// nf -- new function
// ni -- new interface

nt MyStruct struct {
	field1 Int^;
	field2 Long;
}; // I am conflicted about requireing semicolons after types

ni Addable {
	nt Other;
	nt Return;
	nf add(lhs This, rhs Other) Return;
};

// interfaces -- only used for generics; polymorphism is not allowed
ni Cloneable {
	nf clone(this This) This;
};

nf clone_mystruct(s MyStruct) MyStruct {
	MyStruct {
		field1 = s.field1,
		field2 = s.field2,
	} // return is not needed for the last expr if you don't end it with a semicolon
};

prove MyStruct Cloneable {
	clone = clone_mystruct;
};

nf call_clone<T Cloneable>(item T) T {
	imp Cloneable::clone;
	new T := clone(item);
	return new;
};

// gerneric types
nt WrappedCloneable<T Cloneable Interface2able> struct {
	t T;
};

// optionals
nt Null /* defined by compiler I think */;
nt Nothing errtype Null;
nt Optional<T> T $ Null;

// types defined like this implicitly downcast to the inner type
// and the inner type can be upcast explicitly without errors
nt A Null;

// errors
nt MyIntError errtype I32;
nt MyStructError errtype MyStruct;
nt InlineStructError errtype struct { /* ... */ };

glbl nf main(i I32, ii I32) Size {
	nf nested_function() { };
	clojure func(I32, I32) I32 := nf(p1 I32, p2 I32) I32 {
		puts("called clojure");
	};
	puts("hello world");
	var I32 := abcd ? {
		42
	} : {
		24
	};
	var = abcd ? 42 : 24;

	my_list std::List<I32> := a;//std::listof([1, 2, 3]);
	std::for(my_list, nf {
		std::puti(this);
	});

	/* std::for(my_list): nf {
		std::puti(this);
	}; */

	iterator Iter<I32> := std::iter(my_list);
	map Map<Iter<int>> := std::map(iteator, nf(i I32) { i * 2 });

	loop {
	        cond ? {
			break;
		}
	};

	/*std::do(): nf {
		// loop code
		cond
	};*/

	ptr I32^ := &1234;
	derefed_ptr I32 := ptr^;

	asdsad ? {

	} : {

	};
};

nf that_retusn() Optinal<I32> {
    rand := std::random_int_range(0, 10);
    rand < 5 ? {
        return Optional{a};
    } : {
        return Optional{rand}
    };
};

nf func_that_returns_an_optional() Optional<I32> {
	rand := std::random_int_range(0, 10);
	rand < 5 ? {
		return Optional{Null};
	} : {
		return Optional{rand};
	};
};

nf error_handleing(i Int) I32 $ MyIntError | IoError {
	gensym := nf() I32 $ IoError { gensym() };
        a := (f());
	return match f() {
		Nothing {
			MyIntError{42}
		},
		i Int {
			std::write_int_to_file(i)$;
			return i;
		},
	}
};
