// pest grammar for nf

WHITESPACE = _{ " " | "\t" | "\n" }
COMMENT = _{ cpp_style_comment | c_style_comment }
cpp_style_comment = _{  ( "//" ~ (!NEWLINE ~ ANY)* ) }
c_style_comment = _{ "/*" ~ (!"*/" ~ ANY)* ~ "*/" }

/// atoms

ident = @{ !keyword ~ _ident }
_ident = @{ (ASCII_ALPHA | "_") ~ (ASCII_ALPHANUMERIC | "_")* }
ident_path = { ident ~ (op_ident_path_separator ~ ident)* ~ generic_usage? }

number = ${ hex_number | oct_number | bin_number | decimal_number | decimal_integer  }
decimal_integer = @{ ASCII_DIGIT+ }
decimal_number = @{
    (
        (ASCII_DIGIT+ ~ "." ~ ASCII_DIGIT*)
        | (ASCII_DIGIT* ~ "." ~ ASCII_DIGIT+)
    )
    ~ decimal_number_exp?
    | (ASCII_DIGIT+ ~ decimal_number_exp)
}
decimal_number_exp = @{ ( "e" ~ (op_add | op_sub)? ~ ASCII_DIGIT+ ) }
hex_number = @{ "0x" ~ ASCII_HEX_DIGIT+ }
oct_number = @{ "0o" ~ ASCII_OCT_DIGIT+ }
bin_number = @{ "0b" ~ ASCII_BIN_DIGIT+ }

string = ${ "\"" ~ char_contents* ~ "\"" }
char = @{ "'" ~ char_contents ~ "'" }
char_contents = {
    !("\"" | "\\") ~ ANY
    | ("\\" ~ ("\"" | "'" | "\\" | "/" | "b" | "f" | "n" | "r" | "t" | "0"))
    | ("\\" ~ ("u" ~ ASCII_HEX_DIGIT{4}))
}

/// keywords

kw_imp = _{ "imp" }
kw_glbl = { "glbl" }
kw_nf = _{ "nf" }
kw_nt = _{ "nt" }
kw_ni = _{ "ni" }
kw_prove = _{ "prove" }
kw_module = _{ "module" }

kw_if = _{ "if" }
kw_else = _{ "else" }
kw_match = _{ "match" }
kw_loop = _{ "loop" }
kw_break = _{ "break" }
kw_return = _{ "return" }

kw_errtype = _{ "errtype" }
kw_struct = _{ "struct" }
kw_func = _{ "func" }

kw_true = { "true" }
kw_false = { "false" }

keyword = _{
    (kw_imp | kw_glbl | kw_nf | kw_nt | kw_ni | kw_prove | kw_if | kw_else
    | kw_match | kw_loop | kw_break | kw_return | kw_errtype | kw_struct
    | kw_func | kw_module | kw_true | kw_false) ~ !(ASCII_ALPHANUMERIC | "_")
}

/// operators

op_ternary_left = { "?" }
op_ternary_right = { ":" }

// precedence is handled in the rust code
op_binary = _{
    op_add | op_sub | op_mul | op_div | op_mod | op_or | op_bitor | op_and | op_bitand
    | op_bitxor | op_shl | op_shr | op_eq | op_lt | op_gt | op_lteq | op_gteq | op_set
}
op_add = { "+" }
op_sub = { "-" }
op_mul = { "*" }
op_div = { "/" }
op_mod = { "%" }
op_or = { "||" }
op_bitor = { op_bar }
op_and = { "&&" }
op_bitand = { "&" }
op_bitxor = { "~|" }
op_shl = { "<<" }
op_shr = { ">>" }
op_eq = { "==" }
op_lt = { "<" }
op_lteq = { "<=" }
op_gt = { ">" }
op_gteq = { ">=" }

// TODO: bit n= operators
op_set = { op_set_simple | op_plus_set | op_minus_set | op_mul_set | op_div_set }
op_set_simple = { "=" }
op_plus_set = { "+=" }
op_minus_set = { "-=" }
op_mul_set = { "*=" }
op_div_set = { "/=" }

op_prefix = _{ op_not | op_bitnot | op_addrof | op_add | op_sub }
op_not = { "!" }
op_bitnot = { "~" }
op_addrof = { "&" }

op_postfix = _{ op_deref | op_exception }
op_deref = { "^" }
op_exception = { "$" }

// misc operators
op_bar = { "|" }
op_errdef = { "$" }
op_ident_path_separator = { "::" }
op_dot = { "." }

/// generics

generic = {
    "<"
        ~ (generic_decl ~ ",")*
        ~ (generic_decl ~ ","?)
    ~ ">"
}
generic_decl = { ident ~ ident_path* }

generic_usage = {
    "<"
        ~ (ident_path ~ ",")*
        ~ (ident_path ~ ","?)?
    ~ ">"
}

/// type expressions

typeusage_full = {
        typeusage_func
    | ( ident_path ~ (op_deref | typeusage_union | typeusage_err )? )
}
typeusage = {
    typeusage_func
    | ( ident_path ~ (op_deref | typeusage_union)? )
}

typeusage_union = { op_bar ~ (typeusage) }
typeusage_err = { op_errdef ~ (typeusage) }

typeusage_func = { kw_func ~ "(" ~ typeusage_func_param_list ~ ")" ~ typeusage_full? }
typeusage_func_param_list = {
    (typeusage ~ ",")*
    ~ (typeusage ~ ","?)?
}

typexpr_struct = {
    kw_struct ~ "{"
        ~ (struct_field ~ ";")*
    ~ "}"
}
struct_field = { ident ~ typeusage }

typexpr_errtype = { kw_errtype ~ typexpr }

typexpr = {
        typexpr_struct
    | typexpr_errtype
    // TODO: should this be typeusage_full or just typeusage?
    | typeusage_full
}

/// annotations

annotation = { "@" ~ ident ~ "(" ~ annotation_contents ~ ")" }
annotation_contents = @{ (!("(" | ")" | "\0") ~ ASCII)* }

/// expressions

expr_imp = {
    kw_imp ~ ident_path ~ (op_ident_path_separator ~ "*")?
}

expr_funcprototype = {
    kw_glbl? ~ kw_nf ~ ident ~ generic? ~ "(" ~ func_param_list ~ ")" ~ typeusage_full?
}

expr_funcdef = {
    expr_funcprototype ~ expr_block
}
func_param_list = {
        (func_param ~ ",")*
        ~ (func_param ~ ","?)?
}
func_param = { ident ~ typeusage }

expr_newtype = {
    kw_glbl? ~ kw_nt ~ ident ~ generic? ~ typexpr?
}

expr_loop = {
    kw_loop ~ ( "(" ~ expr ~ ")" )? ~ expr
}

expr_match = {
    kw_match ~ expr ~ "{" ~ expr_match_items ~ "}"
}
expr_match_items = { (expr_match_item ~ ",")* ~ (expr_match_item ~ ",")? }
expr_match_item = {
    ((ident ~ typeusage) | typeusage)
    ~ expr_block
}

expr_closure = {
    kw_nf ~ ("(" ~ func_param_list ~ ")")? ~ typeusage_full? ~ expr_block
}

expr_constructor = {
    ident_path ~ "{"
        ~ constructor_args
    ~ "}"
}
constructor_args = {
    (constructor_item ~ ",")*
    ~ (constructor_item ~ ","?)?
}
constructor_item = { (ident ~ op_set_simple ~ expr) | expr }

expr_return = { kw_return ~ expr }
expr_break = { kw_break }

expr_let = { ident ~ typeusage? ~ ":=" ~ expr }

expr_math = { m_expr_ternary }

m_expr_ternary = { m_expr_binary ~ (op_ternary_left ~ expr ~ (op_ternary_right ~ expr)?)* }
m_expr_binary = { m_expr_prefix ~ (op_binary ~ m_expr_prefix)* }
m_expr_prefix = { op_prefix ~ m_expr_prefix | m_expr_postfix }
m_expr_postfix = { (m_expr_funcall ~ op_postfix*) | m_expr_funcall }
m_expr_funcall = { m_expr_atom ~ ("(" ~ funcall_args ~ ")" | (op_dot ~ ident))*  }
funcall_args = {
    (expr ~ ",")*
    ~ (expr ~ ","?)?
}
m_expr_atom = {
    number
    | ident_path
    | string
    | char
    | kw_true
    | kw_false
    | expr_group
}

expr_group = {
    expr_block | ("(" ~ expr ~ ")" )
}

// Currently we require a semicolon after every expr. Should we make this
// behavior more consistant with topexpr? Or make topexpr more consistant with expr?
//
// RESOLVED: both topexprs and exprs need semicolons unless it is an implicit function return
expr_block = {
    "{" ~ (expr ~ ";")* ~ expr? ~ "}"
}

expr = {
    annotation?
    ~ (
        expr_funcdef
        | expr_funcprototype
        | expr_newtype
        | expr_imp
        | expr_let
        | expr_loop
        | expr_match
        | expr_closure
        | expr_return
        | expr_break
        | expr_constructor
        | expr_math
        | expr_group
    )
}

/// top level expressions

topexpr_newinterface = {
    kw_glbl? ~ kw_ni ~ ident ~ "{"
        ~ interface_decl*
    ~ "}"
}
interface_decl = {
    (expr_funcprototype ~ ";")
    | (kw_nt ~ ident ~ ";")
}

topexpr_proof = {
    kw_prove ~ ident_path ~ ident_path ~ "{"
        ~ proof_item*
    ~ "}"
}
proof_item = { ident ~ "=" ~ ident_path ~ ";" }

// this must be first in the program but it is permitted in any order in this
// grammar so we can provide a better error. In a "official" grammar this would
// not be a topexpr but rather be an generic before it in the file rule.
topexpr_moduledecl = {
    kw_module ~ ident_path
}

topexpr = {
    annotation*
    ~ (
        expr_funcdef
        | expr_funcprototype
        | topexpr_newinterface
        | topexpr_proof
        | expr_imp
        | expr_newtype
        | topexpr_moduledecl
    ) ~ ";"
}

file = _{
    SOI ~ topexpr* ~ EOI
}
