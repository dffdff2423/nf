#!/bin/sh
set -ex
cd $(dirname $0)
as "start.s" -o "start.o"
cc -nostdlib -nostartfiles -fno-stack-protector -nolibc intrinsics.c -c -o intrinsics.o
