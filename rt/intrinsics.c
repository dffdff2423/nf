// vim: noexpandtab

#include <sys/types.h>
#include <stdint.h>
#include <linux/unistd.h>

#define STDOUT 1

extern long
__intrinsic_syscall3(long sysnum, long arg0, long arg1, long arg2)
{
	long ret;
	__asm__ (
		"movq %[sysnum], %%rax\n\t"
		"movq %[a0], %%rdi\n\t"
		"movq %[a1], %%rsi\n\t"
		"movq %[a2], %%rdx\n\t"
		"syscall\n\t"
		"movq %%rax, %[ret]"
		: [ret] "=r" (ret)
		: [sysnum] "r" (sysnum), [a0] "r" (arg0), [a1] "r" (arg1), [a2] "r" (arg2)
		: "rax", "rdi", "rsi", "rdx"
	);
	return ret;
}

static ssize_t
write(int fd, const void *buf, size_t count)
{
	return __intrinsic_syscall3(__NR_write, fd, (long)buf, (long)count);
}

extern void
__intrinsic_dumpchar(uint32_t ch)
{
	uint8_t c = ch;
	write(STDOUT, &c, 1);
}
