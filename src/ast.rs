use std::fmt;

use crate::util::SrcSpan;

// Atoms //

pub use crate::ident::*;

#[derive(Clone, Eq)]
pub struct Ident {
    txt: String,
    loc: Option<SrcSpan>,
}

impl Ident {
    pub fn without_span(txt: String) -> Self {
        Self { txt, loc: None }
    }

    #[inline]
    pub fn new(txt: String, loc: SrcSpan) -> Self {
        Self { txt, loc: Some(loc) }
    }

    #[inline]
    pub fn as_str(&self) -> &str {
        return &self.txt;
    }

    #[inline]
    pub fn loc(&self) -> Option<SrcSpan> {
        self.loc
    }

    pub fn into_path(self) -> IdentPath {
        let loc = self.loc;
        let p = vec![self];
        IdentPath { path: p, generic: None, loc, }
    }
}

impl fmt::Debug for Ident {
    // I know that this skips out on SrcSpan, but it makes the ast dump _much_ easier to read
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Ident({})", self.txt)
    }
}

impl fmt::Display for Ident {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.txt)
    }
}

impl std::hash::Hash for Ident {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.txt.hash(state);
        //self.loc.hash(state); we don't hash loc because we want two idents with a diffretnt loc to collide
    }
}

impl std::cmp::PartialEq for Ident {
    fn eq(&self, other: &Self) -> bool {
        // see above
        self.txt == other.txt // && self.loc == other.loc
    }
}

#[derive(Clone, Eq)]
pub struct IdentPath {
    pub path: Vec<Ident>,
    pub generic: Option<GenericUsage>,
    pub loc: Option<SrcSpan>,
}

impl IdentPath {
    pub fn clone_join(&self, other: Ident) -> IdentPath {
        let mut new = self.clone();
        new.path.push(other);
        return new;
    }
}

impl fmt::Debug for IdentPath {
    // see note for ident above
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "IdentPath({self})")
        //f.write_str("IdentPath(")?;

        //f.write_str(")")
    }
}

impl fmt::Display for IdentPath {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for (i, ident) in self.path.iter().enumerate() {
            write!(f, "{ident}")?;
            if i != self.path.len() - 1 {
                f.write_str("::")?;
            }
        }
        Ok(())
    }
}

impl std::hash::Hash for IdentPath {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.path.hash(state);
        self.generic.hash(state);
        // self.loc.hash(state); see above
    }
}

impl std::cmp::PartialEq for IdentPath {
    fn eq(&self, other: &Self) -> bool {
        // see above
        self.path == other.path && self.generic == other.generic // && self.loc == other.loc
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Number {
    I8(i8),
    I16(i16),
    I32(i32),
    I64(i64),
    U8(i16),
    U16(u16),
    U32(u32),
    U64(u64),
    Size(usize),
    SSize(isize),
    Float(f32),
    Double(f64),
}

#[derive(Debug, Clone, PartialEq)]
pub enum Atom {
    Number(Number),
    String(String),
    IdentPath(IdentPath),
    Char(char),
}

// Generics //

#[derive(Debug, Clone, Hash, PartialEq, Eq)]
pub struct GenericUsage {
    pub generics: Vec<IdentPath>,
    pub loc: SrcSpan,
}

#[derive(Debug, Clone, Hash, PartialEq, Eq)]
pub struct GenericDecl {
    pub loc: SrcSpan,
    // TODO
}

// type expressions //

#[derive(Debug, Clone, Hash, PartialEq, Eq)]
pub enum TypeExpr {
    Struct(),
    Errtype(),
    TaggedUnion(),
    ErrorUnion(),
}

/// TODO
#[derive(Debug, Clone, Hash, PartialEq, Eq)]
pub enum TypeUsage {
    Void
}

/// [`TypeUsage`] but with error unions added, should only be used in [`FunctionPrototype`]s
/// TODO
#[derive(Debug, Clone, Hash, PartialEq, Eq)]
pub struct TypeUsageFull {
    loc: SrcSpan,
    main: TypeUsage,
    error: Option<TypeUsage>,
}

impl TypeUsageFull {
    pub fn void(loc: SrcSpan) -> Self {
        TypeUsageFull { loc, main: TypeUsage::Void, error: None }
    }
    pub fn is_void(&self) -> bool {
        self.main == TypeUsage::Void && self.error.is_none()
    }
}

// Annotation //

// TODO
#[derive(Debug, Clone, Hash, PartialEq)]
pub struct Annotation {
    pub loc: SrcSpan,
    pub name: Ident,
    pub contents: String,
}

// Expressions //

#[derive(Debug, Clone, Hash, PartialEq)]
pub struct Import {
    pub loc: SrcSpan,
}

#[derive(Debug, Clone, Hash, PartialEq)]
pub struct NewType {
    pub loc: SrcSpan,
    pub name: String,
    pub expr: TypeExpr,
}


/// Function or prototype
#[derive(Debug, Clone, PartialEq)]
pub struct Function {
    pub loc: SrcSpan,
    pub proto: FunctionPrototype,
    pub exprs: BlockExpr,
}

#[derive(Debug, Clone, PartialEq)]
pub struct FunctionPrototype {
    pub loc: SrcSpan,
    pub global: bool,
    pub name: Ident,
    pub generic: Option<GenericDecl>,
    pub params: Vec<FunctionParam>,
    pub ret: TypeUsageFull,
}

#[derive(Debug, Clone, PartialEq)]
pub struct FunctionParam {
    pub loc: SrcSpan,
    pub name: Ident,
    pub ty: TypeUsage,
}

#[derive(Debug, Clone, PartialEq)]
pub struct BlockExpr {
    pub exprs: Vec<Expr>,
}

#[derive(Debug, Clone, Copy, PartialEq)]
#[repr(u8)]
pub enum BinOp {
    Add = b'+',
    Sub = b'-',
    Mul = b'*',
    Div = b'/',
    /* TODO... */
}

/// Binary expression
#[derive(Debug, Clone, PartialEq)]
pub struct BinExpr {
    pub lhs: Expr,
    pub op: BinOp,
    pub rhs: Expr,
}

#[derive(Debug, Clone, PartialEq)]
pub struct FuncallExpr {
    pub name: IdentPath,
    pub args: Vec<Expr>,
}

#[derive(Debug, Clone, PartialEq)]
pub struct ElementGetExpr {
    pub base: Box<Expr>,
    pub elem: Ident,
}

#[derive(Debug, Clone, PartialEq)]
pub enum Exprs {
    Import(Import),
    NewType(NewType),
    Function(Function),
    Block(BlockExpr),
    BinExpr(Box<BinExpr>),
    Funcall(FuncallExpr),
    ElementGetExpr(ElementGetExpr),
    Atom(Atom),
}

#[derive(Debug, Clone, PartialEq)]
pub struct Expr {
    pub loc: SrcSpan,
    pub annotations: Vec<Annotation>,
    pub expr: Exprs,
}

#[allow(unused_variables)] // FIXME
pub trait ExprVisitor<T> {
    fn on_import(&mut self, meta: &Expr, imp: &Import) -> T { todo!(); }
    fn on_function(&mut self, meta: &Expr, f: &Function) -> T;
    fn on_function_proto(&mut self, meta: &Expr, f: &FunctionPrototype) -> T;
    fn on_funcall(&mut self, meta: &Expr, f: &FuncallExpr) -> T;
    fn on_newtype(&mut self, meta: &Expr, nt: &NewType) -> T { todo!(); }
    fn on_bin_expr(&mut self, meta: &Expr, bin: &BinExpr) -> T;
    fn on_element_get(&mut self, meta: &Expr, el: &ElementGetExpr) -> T { todo!() }
    fn on_atom(&mut self, meta: &Expr, atom: &Atom) -> T;
    fn on_block(&mut self, meta: &Expr, blk: &BlockExpr) -> T;
}

pub fn visit_expr<T>(vtor: &mut impl ExprVisitor<T>, expr: &Expr) -> T {
    use Exprs::*;
    match &expr.expr {
        Import(i) => vtor.on_import(expr, i),
        NewType(i) => vtor.on_newtype(expr, i),
        Function(i) => vtor.on_function(expr, i),
        Block(i) => vtor.on_block(expr, i),
        BinExpr(i) => vtor.on_bin_expr(expr, i),
        Funcall(i) => vtor.on_funcall(expr, i),
        ElementGetExpr(i) => vtor.on_element_get(expr, i),
        Atom(i) => vtor.on_atom(expr, i),
    }
}

// Top Expressions //

#[derive(Debug, Clone, PartialEq)]
pub enum TopExprs {
    Function(Function),
    FunctionPrototype(FunctionPrototype),
    Interface(),
    Prove(),
    Import(Import),
    Type(NewType),
}

#[derive(Debug, Clone, PartialEq)]
pub struct TopExpr {
    pub loc: SrcSpan,
    pub annotations: Vec<Annotation>,
    pub expr: TopExprs
}

#[derive(Debug, Clone, PartialEq)]
pub struct File<'a> {
    /// Path to the file that this module was parsed from
    pub path: Option<&'a str>,
    /// Source code
    pub txt: &'a str,
    /// Name given in module declaration
    pub module: Option<IdentPath>,
    pub exprs: Vec<TopExpr>,
}

#[allow(unused_variables)] // FIXME
pub trait TopExprVisitor<T> {
    fn on_function(&mut self, meta: &TopExpr, f: &Function) -> T;
    fn on_function_proto(&mut self, meta: &TopExpr, f: &FunctionPrototype) -> T;
    fn on_interface(&mut self, meta: &TopExpr, ) -> T { todo!(); }
    fn on_prove(&mut self, meta: &TopExpr, ) -> T { todo!(); }
    fn on_import(&mut self, meta: &TopExpr, imp: &Import) -> T { todo!(); }
    fn on_newtype(&mut self, meta: &TopExpr, nt: &NewType) -> T { todo!(); }
}

pub fn visit_top_expr<T>(vtor: &mut impl TopExprVisitor<T>, expr: &TopExpr) -> T {
    use TopExprs::*;
    match &expr.expr {
        Function(f) => vtor.on_function(expr, f),
        FunctionPrototype(p) => vtor.on_function_proto(expr, p),
        Interface() => vtor.on_interface(expr, ),
        Prove() => vtor.on_prove(expr, ),
        Import(imp) => vtor.on_import(expr, imp),
        Type(nt) => vtor.on_newtype(expr, nt),
    }
}
