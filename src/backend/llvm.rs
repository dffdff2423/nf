use std::path::PathBuf;

use inkwell::OptimizationLevel;
use inkwell::builder::Builder;
use inkwell::context::Context as LLVMCtx;
use inkwell::module::Linkage;
use inkwell::module::Module as LLVMModule;
use inkwell::targets::{FileType, InitializationConfig, Target, TargetTriple, RelocMode, CodeModel};
use inkwell::values::{FunctionValue, BasicMetadataValueEnum, InstructionValue};

use crate::ast::{self, IdentPath, Ident};
use crate::ir;

pub type CodegenResult = Result<(), Box<dyn std::error::Error>>;

pub fn init_llvm() {
    let init = InitializationConfig {
        asm_parser: false,
        asm_printer: true,
        base: true,
        disassembler: false,
        info: true,
        machine_code: true,
    };
    Target::initialize_x86(&init);
}

pub struct CodegenConfig {
    pub emit_llvm: bool,
    pub emit_asm: bool,
    pub output_path: PathBuf,
}

pub fn codegen(m: &ir::Module, mut conf: CodegenConfig) -> CodegenResult {
    let llvm = LLVMCtx::create();
    let mut ctx = CodegenCtx::new(&llvm, m);

    let triple = TargetTriple::create("x86_64-pc-linux-gnu");
    let tget = Target::from_triple(&triple)?;
    // TODO: make these into flags
    let mech = tget.create_target_machine(&triple, "x86-64", ""
            , OptimizationLevel::None, RelocMode::Static, CodeModel::Default)
        .unwrap();
    let mech_data = mech.get_target_data();
    ctx.llmod.set_triple(&mech.get_triple());
    ctx.llmod.set_data_layout(&mech_data.get_data_layout());

    for fun in &ctx.irmod.functions {
        if ctx.llfunmap.contains_key(&fun.name) {
            todo!("handle duplicate functions");
        }
        emit_proto(&mut ctx, fun);
    }

    for fun in &ctx.irmod.functions {
        emit_fun(&mut ctx, fun);
    }

    if let Err(e) = ctx.llmod.verify() {
        eprintln!("Failed to verify llvm module:\n");
        eprintln!("{e}");
        std::process::exit(1);
    }

    if conf.emit_llvm {
        conf.output_path.set_extension("ll");
        ctx.llmod.print_to_file(&conf.output_path)?;
    }

    if conf.emit_asm {
        conf.output_path.set_extension("s");
        mech.write_to_file(&ctx.llmod, FileType::Assembly, &conf.output_path)?;
    }

    if conf.emit_asm || conf.emit_llvm {
        return Ok(());
    }

    mech.write_to_file(&ctx.llmod, FileType::Object, &conf.output_path)?;

    Ok(())
}

fn emit_proto(ctx: &mut CodegenCtx, fun: &ir::Function) {
    eprintln!("emmiting proto: {}", fun.name);
    let void = ctx.llvm.void_type();
    let i32 = ctx.llvm.i32_type();
    let fnty = void.fn_type(&[i32.into()], false);

    let linkage = if fun.external {
        Linkage::External
    } else {
        Linkage::Private
    };
    let llfun = ctx.llmod.add_function(&format!("{}", fun.name), fnty, Some(linkage));
    ctx.llfunmap.insert(fun.name.clone(), llfun);
}

fn emit_fun(ctx: &mut CodegenCtx, fun: &ir::Function) {
    eprintln!("emmiting function: {}", fun.name);
    let llfun = ctx.llfunmap[&fun.name];

    for blk in &fun.blks {
        let bb = ctx.llvm.append_basic_block(llfun, "");
        ctx.builder.position_at_end(bb);
        emit_blk(ctx, blk);
    }
    ctx.builder.build_return(None);
}

fn emit_blk(ctx: &mut CodegenCtx, blk: &ir::Block) {
    ctx.curr_block = Vec::new();
    for instr in &blk.instr {
        match instr {
            ir::Instruction::FunCall(fc) => {
                emit_funcall(ctx, fc);
            },
            ir::Instruction::Add { lhs, rhs } => {
                let v = ctx.builder.build_int_add(
                    getval!(ctx, *lhs).into_int_value()
                    , getval!(ctx, *rhs).into_int_value(), "");
                ctx.curr_block.push(make_iv(v.into()));
            }
            ir::Instruction::Sub { lhs, rhs } => {
                let v = ctx.builder.build_int_sub(
                    getval!(ctx, *lhs).into_int_value()
                    , getval!(ctx, *rhs).into_int_value(), "");
                ctx.curr_block.push(make_iv(v.into()));
            }
            ir::Instruction::Mul { lhs, rhs } => {
                let v = ctx.builder.build_int_mul(
                    getval!(ctx, *lhs).into_int_value()
                    , getval!(ctx, *rhs).into_int_value(), "");
                ctx.curr_block.push(make_iv(v.into()));
            }
            ir::Instruction::SDiv { lhs, rhs } => {
                let v = ctx.builder.build_int_signed_div(
                    getval!(ctx, *lhs).into_int_value()
                    , getval!(ctx, *rhs).into_int_value(), "");
                ctx.curr_block.push(make_iv(v.into()));
            }
        }
    }
}

fn emit_funcall(ctx: &mut CodegenCtx, fc: &ir::FunCall) {
    let mut args = Vec::new();
    for arg in &fc.args {
        args.push(getval!(ctx, *arg));
    }
    let v = ctx.builder.build_call(ctx.llfunmap[&fc.target], &args, "");
    ctx.curr_block.push(match v.try_as_basic_value() {
        either::Left(v) => make_iv(v.into()),
        either::Right(v) => EmmitedInstrVal::Other(v),
    });
}

macro_rules! getval {
    ($ctx:tt, $($v:tt)*) => {
        _getval($ctx.llvm, &$ctx.curr_block, $($v)*)
    };
}
pub(self) use getval;

// we need to take 2 args here instead of just the ctx for dumb lifetime reaons
fn _getval<'a>(llvm: &'a LLVMCtx, blk: &[EmmitedInstrVal<'a>], v: ir::Val) -> BasicMetadataValueEnum<'a> {
    match v {
        ir::Val::Number(n) => match n {
            ast::Number::I32(i) => {
                let i32 = llvm.i32_type();
                i32.const_int(i as u64, false).into()
            }
            _ => todo!("numbers other than i32"),
        },
        ir::Val::Void => todo!("emit val Void"),
        ir::Val::CurrBlockRef(idx) => {
            match blk[idx] {
                EmmitedInstrVal::Basic(b) => b,
                EmmitedInstrVal::Other(o) => panic!("expected basic value but got: {o}"),
            }
        }
    }
}

#[inline]
fn make_iv(basic: BasicMetadataValueEnum) -> EmmitedInstrVal {
    EmmitedInstrVal::Basic(basic)
}

#[derive(Debug, Clone, Copy)]
enum EmmitedInstrVal<'ctx> {
    Basic(BasicMetadataValueEnum<'ctx>),
    Other(InstructionValue<'ctx>),
}

struct CodegenCtx<'ctx> {
    llvm: &'ctx LLVMCtx,
    llfunmap: ir::IrMap<IdentPath, FunctionValue<'ctx>>,
    irmod: &'ctx ir::Module<'ctx>,
    llmod: LLVMModule<'ctx>,
    builder: Builder<'ctx>,
    curr_block: Vec<EmmitedInstrVal<'ctx>>
}

impl<'ctx> CodegenCtx<'ctx> {
    fn new(llvm: &'ctx LLVMCtx, irmod: &'ctx ir::Module<'ctx>) -> CodegenCtx<'ctx> {
        let mut ctx = CodegenCtx {
            llvm,
            llfunmap: ir::IrMap::new(),
            irmod,
            llmod: llvm.create_module(irmod.srcname.unwrap_or("")),
            builder: llvm.create_builder(),
            curr_block: Vec::new(),
        };
        fill_intrinsic_protos(&mut ctx);
        return ctx;
    }
}

fn fill_intrinsic_protos<'a>(ctx: &mut CodegenCtx) {
    let ns = Ident::without_span("__intrinsic".to_string()).into_path();

    let i32 = ctx.llvm.i32_type();
    let void = ctx.llvm.void_type();

    let dumpchar = ns.clone_join(Ident::without_span("dumpchar".to_string()));
    let dumpchar_proto_ty = void.fn_type(&[i32.into()], false);
    let dumpchar_proto = ctx.llmod.add_function(
            "__intrinsic_dumpchar", dumpchar_proto_ty, Some(Linkage::External));
    ctx.llfunmap.insert(dumpchar, dumpchar_proto);
}
