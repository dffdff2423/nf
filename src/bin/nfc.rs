use std::error::Error;
use std::fs;

use nf::parse;
use nf::irgen;

use argh::FromArgs;

/// Compile nf programs
#[derive(FromArgs)]
struct Config {

    /// dump internal ast structures to stderr, output is unstable
    #[argh(switch)]
    dump_ast: bool,

    /// dump internal ir structures to stderr, output is unstable
    #[argh(switch)]
    dump_ir: bool,

    /// emit llvm ir instead of writing an object file. This option can be used with --emit-asm
    #[argh(switch)]
    emit_llvm: bool,

    /// emit assembly instead of writing an object file. This option can be used with --emit-llvm
    #[argh(switch)]
    emit_asm: bool,

    /// file to compile
    #[argh(positional)]
    file: std::path::PathBuf,

    /// output file
    #[argh(option, short = 'o')]
    output: Option<std::path::PathBuf>,
}

fn try_main() -> Result<(), Box<dyn Error>> {
    let conf: Config = argh::from_env();
    let txt = fs::read_to_string(&conf.file)?;

    let fstr = conf.file.to_str().unwrap();
    let file = parse::parse(&txt, Some(fstr))?;

    if conf.dump_ast {
        dump_ast(&file);
    }

    let ir = irgen::gen_ir(&file)?;

    if conf.dump_ir {
        dump_ir(&ir);
    }

    nf::backend::llvm::init_llvm();
    let out = if let Some(o) = &conf.output {
        o.clone()
    } else {
        let mut o = conf.file.clone();
        o.set_extension("o");
        o
    };
    let codegen_conf = nf::backend::llvm::CodegenConfig {
        emit_llvm: conf.emit_llvm,
        emit_asm: conf.emit_asm,
        output_path: out,
    };
    nf::backend::llvm::codegen(&ir, codegen_conf)?;

    Ok(())
}

fn main() {
    match try_main() {
        Ok(()) => (),
        Err(e) => {
            eprintln!("Error:{e}");
            std::process::exit(1);
        }
    }
}

fn dump_ast(f: &nf::ast::File) {
    if let Some(m) = &f.module {
        eprintln!("MODULE: {m};");
    }
    eprintln!("EXPRESSIONS:");
    for expr in &f.exprs {
        eprintln!("{expr:#?}");
    }
}

fn dump_ir(f: &nf::ir::Module) {
    eprintln!("IR:\n{f:#?}");
}

