use std::fmt;

use crate::ast::GenericUsage;
use crate::util::SrcSpan;

#[derive(Clone, Eq)]
#[allow(unused)] // FIXME
pub struct Ident {
    txt: String,
    loc: Option<SrcSpan>,
}

#[allow(unused)] // FIXME
impl Ident {
    pub fn without_span(txt: String) -> Self {
        Self { txt, loc: None }
    }

    #[inline]
    pub fn new(txt: String, loc: SrcSpan) -> Self {
        Self { txt, loc: Some(loc) }
    }

    #[inline]
    pub fn as_str(&self) -> &str {
        return &self.txt;
    }

    #[inline]
    pub fn loc(&self) -> Option<SrcSpan> {
        self.loc
    }

    pub fn into_path(self) -> IdentPath {
        let loc = self.loc;
        let p = vec![self];
        IdentPath { path: p, generic: None, loc, }
    }
}

impl fmt::Debug for Ident {
    // I know that this skips out on SrcSpan, but it makes the ast dump _much_ easier to read
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Ident({})", self.txt)
    }
}

impl fmt::Display for Ident {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.txt)
    }
}

impl std::hash::Hash for Ident {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.txt.hash(state);
        //self.loc.hash(state); we don't hash loc because we want two idents with a diffretnt loc to collide
    }
}

impl std::cmp::PartialEq for Ident {
    fn eq(&self, other: &Self) -> bool {
        // see above
        self.txt == other.txt // && self.loc == other.loc
    }
}

#[derive(Clone, Eq)]
pub struct IdentPath {
    pub path: Vec<Ident>,
    pub generic: Option<GenericUsage>,
    pub loc: Option<SrcSpan>,
}

#[allow(unused)] // FIXME
impl IdentPath {
    pub fn clone_join(&self, other: Ident) -> IdentPath {
        let mut new = self.clone();
        new.path.push(other);
        return new;
    }
}

impl fmt::Debug for IdentPath {
    // see note for ident above
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "IdentPath({self})")
        //f.write_str("IdentPath(")?;

        //f.write_str(")")
    }
}

impl fmt::Display for IdentPath {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for (i, ident) in self.path.iter().enumerate() {
            write!(f, "{ident}")?;
            if i != self.path.len() - 1 {
                f.write_str("::")?;
            }
        }
        Ok(())
    }
}

impl std::hash::Hash for IdentPath {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.path.hash(state);
        self.generic.hash(state);
        // self.loc.hash(state); see above
    }
}

impl std::cmp::PartialEq for IdentPath {
    fn eq(&self, other: &Self) -> bool {
        // see above
        self.path == other.path && self.generic == other.generic // && self.loc == other.loc
    }
}
