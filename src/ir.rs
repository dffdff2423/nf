use crate::ast;

pub type IrMap<K, V> = std::collections::HashMap<K, V>;

#[derive(Debug, Clone, PartialEq)]
pub struct Module<'a> {
    pub srcname: Option<&'a str>,
    pub functions: Vec<Function>,
    pub funcmap: IrMap<ast::IdentPath, FuncRef>,
    // types: ...
    // globals: ...
    // etc ...
}

impl<'a> Module<'a> {
    pub fn add_function(&mut self, f: Function) {
        self.functions.push(f);
        let idx = self.functions.len() - 1;
        self.funcmap.insert(self.functions[idx].name.clone(), FuncRef::Internal(idx));
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum FuncRef {
    /// A function within this module
    Internal(usize),
}

#[derive(Debug, Clone, PartialEq)]
pub struct Function {
    pub name: ast::IdentPath,
    pub blks: Vec<Block>,
    pub external: bool,
    pub is_proto: bool,
    // pub vars: ...
}

#[derive(Debug, Clone, PartialEq)]
pub struct Block {
    pub instr: Vec<Instruction>,
}

#[derive(Debug, Clone, PartialEq)]
pub enum Instruction {
    FunCall(FunCall),
    Add { lhs: Val, rhs: Val },
    Sub { lhs: Val, rhs: Val },
    Mul { lhs: Val, rhs: Val },
    /// Signed division
    SDiv { lhs: Val, rhs: Val },
}

#[derive(Debug, Clone, PartialEq)]
pub struct FunCall {
    pub target: ast::IdentPath,
    pub args: Vec<Val>,
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Val {
    Number(ast::Number),
    CurrBlockRef(usize),
    Void,
}
