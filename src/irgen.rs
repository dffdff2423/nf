//! Convert ast into ir

use crate::ast;
use crate::ir;
use crate::util::CompileError;

pub type IrResult<T> = Result<T, CompileError>;

pub fn gen_ir<'a>(f: &'a ast::File) -> IrResult<ir::Module<'a>> {
    let module = ir::Module {
        srcname: f.path,
        functions: Vec::new(),
        funcmap: ir::IrMap::new(),
    };
    let mut ctx = IrGenCtx { module, current_block: None, current_function: None };
    for expr in &f.exprs {
        ast::visit_top_expr(&mut ctx, expr)?;
    }
    return Ok(ctx.module);
}

struct IrGenCtx<'a> {
    module: ir::Module<'a>,
    current_function: Option<usize>,
    current_block: Option<usize>,
}

impl<'a> IrGenCtx<'a> {
    fn current_function(&mut self) -> &mut ir::Function {
        let idx = self.current_function.unwrap();
        &mut self.module.functions[idx]
    }

    fn current_block(&mut self) -> &mut ir::Block {
        let idx = self.current_block.unwrap();
        &mut self.current_function().blks[idx]
    }

    fn proccess_function(&mut self, f: &ast::Function) -> IrResult<()> {
        let old_nf = self.current_function;
        let old_blk = self.current_block;

        let mut nf = ir::Function {
            name: f.proto.name.clone().into_path(),
            blks: Vec::new(),
            external: f.proto.global,
            is_proto: false,
        };
        let newblk = ir::Block {
            instr: Vec::new(),
        };
        nf.blks.push(newblk);
        self.module.add_function(nf);

        self.current_function = Some(self.module.functions.len() - 1);
        self.current_block = Some(self.current_function().blks.len() - 1);

        for item in &f.exprs.exprs {
            ast::visit_expr(self, item)?;
        }

        self.current_function = old_nf;
        self.current_block = old_blk;

        Ok(())
    }
}

// WARNING: All visitor functions must stop generating after an error occurs
impl<'a> ast::TopExprVisitor<IrResult<()>> for IrGenCtx<'a> {
    fn on_function(&mut self, _meta: &ast::TopExpr, f: &ast::Function) -> IrResult<()> {
        self.proccess_function(f)
    }

    fn on_function_proto(&mut self, _meta: &ast::TopExpr, _f: &ast::FunctionPrototype) -> IrResult<()> {
        todo!()
    }
}

impl<'a> ast::ExprVisitor<IrResult<ir::Val>> for IrGenCtx<'a> {
    fn on_function(&mut self, _meta: &ast::Expr, _f: &ast::Function) -> IrResult<ir::Val> {
        todo!()
    }

    fn on_function_proto(&mut self, _meta: &ast::Expr, _f: &ast::FunctionPrototype) -> IrResult<ir::Val> {
        todo!()
    }

    fn on_funcall(&mut self, _meta: &ast::Expr, f: &ast::FuncallExpr) -> IrResult<ir::Val> {
        let target = f.name.clone();
        let mut args = Vec::new();
        for ir in &f.args {
            args.push(ast::visit_expr(self, ir)?);
        }

        let blk = self.current_block();
        blk.instr.push(ir::Instruction::FunCall(ir::FunCall {
            target, args,
        }));

        Ok(ir::Val::Void)
    }

    fn on_atom(&mut self, _meta: &ast::Expr, atom: &ast::Atom) -> IrResult<ir::Val> {
        Ok(match atom {
            ast::Atom::Number(num) => ir::Val::Number(*num),
            ast::Atom::String(_) => todo!(),
            ast::Atom::IdentPath(_) => todo!(),
            ast::Atom::Char(_) => todo!(),
        })
    }

    fn on_block(&mut self, _meta: &ast::Expr, _blk: &ast::BlockExpr) -> IrResult<ir::Val> {
        todo!()
    }

    fn on_bin_expr(&mut self, _meta: &ast::Expr, bin: &ast::BinExpr) -> IrResult<ir::Val> {
        use ir::Instruction::*;
        let lhs = ast::visit_expr(self, &bin.lhs)?;
        let rhs = ast::visit_expr(self, &bin.rhs)?;
        let instr = match bin.op {
            // FIXME: this will be very wrong when we have types
            ast::BinOp::Add => Add { lhs, rhs },
            ast::BinOp::Sub => Sub { lhs, rhs },
            ast::BinOp::Mul => Mul { lhs, rhs },
            ast::BinOp::Div => SDiv { lhs, rhs },
        };
        let cb = self.current_block();
        let v = cb.instr.len();
        cb.instr.push(instr);
        return Ok(ir::Val::CurrBlockRef(v));
    }
}
