#![allow(clippy::needless_return)]

pub mod ast;
pub mod backend;
pub mod ir;
pub mod irgen;
pub mod parse;
pub mod util;

mod ident;
