use once_cell::sync::OnceCell;

pub use pest::error::Error as PestError;
use pest::iterators::Pair;
use pest::Parser;
use pest::pratt_parser::PrattParser;

use crate::ast;
use crate::util;

// The error is boxed as suggested by a clippy warning
// https://rust-lang.github.io/rust-clippy/master/index.html#result_large_err
pub type ParseResult<T> = Result<T, Box<PestError<Rule>>>;

#[derive(pest_derive::Parser)]
#[grammar = "../nf.pest"]
struct PestParser;

fn pratt_parser() -> &'static PrattParser<Rule> {
    static PRATT_PARSER: OnceCell<PrattParser<Rule>> = OnceCell::new();
    PRATT_PARSER.get_or_init(|| {
        use pest::pratt_parser::{Assoc::*, Op};
        use Rule::*;

        PrattParser::new()
            .op(Op::infix(op_or, Left))
            .op(Op::infix(op_and, Left))
            .op(Op::infix(op_eq, Left)
                | Op::infix(op_lt, Left) | Op::infix(op_lteq, Left)
                | Op::infix(op_gt, Left) | Op::infix(op_gteq, Left))
            .op(Op::infix(op_bitor, Left))
            .op(Op::infix(op_bitxor, Left))
            .op(Op::infix(op_bitand, Left))
            .op(Op::infix(op_shl, Left) | Op::infix(op_shr, Left))
            .op(Op::infix(op_add, Left) | Op::infix(op_sub, Left))
            .op(Op::infix(op_mul, Left) | Op::infix(op_div, Left) | Op::infix(op_mod, Left))
    })
}

pub fn parse<'a>(txt: &'a str, path: Option<&'a str>) -> ParseResult<ast::File<'a>> {
    let mut parsed_file = PestParser::parse(Rule::file, txt)?;
    let mut module = ast::File { path, txt, module: None, exprs: vec![] };

    // check for module stmt and parse it first if it exist
    let first = parsed_file.peek();
    if let Some(pair) = first {
        for pair in pair.into_inner() {
            match pair.as_rule() {
                Rule::annotation => eprintln!("WARN: maybe module annotation, those are not yet implemented"),
                Rule::topexpr_moduledecl => {
                    let mut inner = pair.into_inner();
                    let path = parse_no_generic_ident_path(inner.next().unwrap())?;
                    assert!(inner.next().is_none());
                    module.module = Some(path);
                    parsed_file.next();
                },
                _r => (/* do nothing, we check this later */),
            }
        }
    }

    for topexpr in parsed_file {
        if topexpr.as_rule() == Rule::EOI {
            break;
        }
        let topexpr = parse_topexpr(topexpr)?;
        module.exprs.push(topexpr);
    }

    Ok(module)
}

// Annotations //

fn parse_annotation(annotation: Pair<Rule>) -> ast::Annotation {
    let loc = annotation.as_span().into();
    let mut annotation = annotation.into_inner();
    let name = parse_ident(annotation.next().expect("expected annotation name"));
    let contents = annotation.next().expect("expected annotation contents").as_str().to_string();
    ast::Annotation { loc, name, contents }
}

// Top Expressions //

fn parse_topexpr(topexpr: Pair<Rule>) -> ParseResult<ast::TopExpr> {
    let span = topexpr.as_span();

    let mut annotations = Vec::new();
    let mut parsed_expr = None;
    for item in topexpr.into_inner() {
        match item.as_rule() {
            Rule::annotation => {
                assert_eq!(parsed_expr, None, "Tried to parse annotation after topexpr");
                annotations.push(parse_annotation(item));
            },
            Rule::expr_funcdef => {
                assert_eq!(parsed_expr, None);
                let nf = parse_funcdef(item)?;
                parsed_expr = Some(ast::TopExprs::Function(nf));
            },
            Rule::expr_funcprototype => {
                assert_eq!(parsed_expr, None);
                let proto = parse_funcprototype(item);
                parsed_expr = Some(ast::TopExprs::FunctionPrototype(proto));
            },
            Rule::topexpr_newinterface => {
                assert_eq!(parsed_expr, None);
                todo!("Rule::topexper_newinterface");
            }
            Rule::topexpr_proof => {
                assert_eq!(parsed_expr, None);
                todo!("Rule::topexpr_proof");
            }
            Rule::expr_imp => {
                assert_eq!(parsed_expr, None);
                util::warn_once!("TODO: Rule::expr_import");
            },
            Rule::expr_newtype => {
                assert_eq!(parsed_expr, None);
                todo!("Rule::expr_newtype");
            },

            other => { panic!("unexpected rule {other:?}"); }
        }
    }
    if parsed_expr.is_none() {
        return Err(Box::new(make_custom_error("Expected expression after annotation", span)));
    }

    Ok(ast::TopExpr { loc: span.into(), annotations, expr: parsed_expr.unwrap() })
}

// Expressions //

fn parse_expr(e: Pair<Rule>) -> ParseResult<ast::Expr> {
    // TODO
    util::warn_once!("WARN: TODO parse_expr");
    let loc = e.as_span().into();
    let e = e.into_inner().next().expect("expected expr");
    let e = parse_math_expr(e)?;
    Ok(ast::Expr {
        loc, annotations: Vec::new(), expr: e
    })
}

fn parse_funcdef(f: Pair<Rule>) -> ParseResult<ast::Function> {
    let loc = f.as_span().into();
    let mut f = f.into_inner();
    let proto = parse_funcprototype(f.next().expect("expected expr_funcprototype"));
    let exprs = parse_block(f.next().expect("expected expr_block"))?;
    Ok(ast::Function { loc, proto, exprs })
}

fn parse_funcprototype(proto: Pair<Rule>) -> ast::FunctionPrototype {
    let loc: util::SrcSpan = proto.as_span().into();

    let mut global = false;
    let mut name = None;
    #[allow(unused_mut)] // NOTE: remove when adding Rule::generic
    let mut generic = None;
    let mut params = None;
    let mut ret = None;
    for item in proto.into_inner() {
        match item.as_rule() {
            Rule::kw_glbl => {
                assert!(!global, "double glbl keyword in funcprototype");
                global = true;
            },
            Rule::ident => {
                assert_eq!(name, None);
                name = Some(parse_ident(item));
            },
            Rule::generic => {
                assert_eq!(generic, None);
                todo!("funcprototype generics");
            },
            Rule::func_param_list => {
                assert_eq!(params, None);
                params = Some(parse_func_param_list(item));
            }
            Rule::typeusage_full => {
                assert_eq!(ret, None);
                ret = Some(parse_typeusage_full(item));
            }
            rest => { panic!("unexpected rule {rest:?}") }
        }
    }

    if ret.is_none() {
        let ss = util::SrcSpan { start: loc.end, end: loc.end };
        ret = Some(ast::TypeUsageFull::void(ss));
    }

    ast::FunctionPrototype {
        loc, global,
        name: name.unwrap(),
        generic,
        params: params.unwrap(),
        ret: ret.unwrap(),
    }
}

fn parse_func_param_list(lst: Pair<Rule>) -> Vec<ast::FunctionParam> {
    let mut r = Vec::new();
    for p in lst.into_inner() {
        r.push(parse_funcparam(p));
    }
    r
}

fn parse_funcparam(p: Pair<Rule>) -> ast::FunctionParam {
    let loc = p.as_span().into();
    let mut p = p.into_inner();
    let name = parse_ident(p.next().expect("expected ident"));
    let ty = parse_typeusage(p.next().expect("expected typeusage"));
    ast::FunctionParam { loc, name, ty }
}

fn parse_math_expr(p: Pair<Rule>) -> ParseResult<ast::Exprs> {
    util::warn_once!("WARN: TODO parse_math_expr");
    let fnexpr = p
        .into_inner().next().unwrap() // if
        .into_inner().next().unwrap() // bin
        ;
    let e = parse_bin_expr(fnexpr);
    return e;
}

fn parse_bin_expr(p: Pair<Rule>) -> ParseResult<ast::Exprs> {
    let loc = p.as_span();
    let p = p.into_inner();
    let e = pratt_parser()
        .map_primary(|pref| {
            parse_prefix_expr(pref)
        })
        .map_infix(|lhs, op, rhs| {
            let op = match op.as_rule() {
                Rule::op_add => ast::BinOp::Add,
                Rule::op_sub => ast::BinOp::Sub,
                Rule::op_mul => ast::BinOp::Mul,
                Rule::op_div => ast::BinOp::Div,
                r => unimplemented!("TODO: handle binop {r:?}")
            };
            let lhs = ast::Expr {
                loc: loc.into(), // FIXME: this loc is incorrect
                annotations: Vec::new(), // TODO
                expr: lhs?,
            };
            let rhs = ast::Expr {
                loc: loc.into(), // FIXME: this loc is incorrect
                annotations: Vec::new(), // TODO
                expr: rhs?,
            };
            Ok(ast::Exprs::BinExpr(Box::new(ast::BinExpr { lhs, op, rhs, })))
        })
        .parse(p)?;
    Ok(e)
}

fn parse_prefix_expr(p: Pair<Rule>) -> ParseResult<ast::Exprs> {
    let funcall = p
        .into_inner().next().unwrap() // postifx
        .into_inner().next().unwrap()
        ;
    parse_funcall_expr(funcall)
}

fn parse_funcall_expr(p: Pair<Rule>) -> ParseResult<ast::Exprs> {
    let mut p = p.into_inner();
    let atom = p.next().expect("expected atom for funcall expression");
    let atom_span = atom.as_span();
    let e = parse_atom_expr(atom);

    if let Some(funcall_or_dot) = p.next() {
        match funcall_or_dot.as_rule() {
            Rule::op_dot => {
                todo!("element access");
            },
            Rule::funcall_args => {
                if let ast::Exprs::Atom(ast::Atom::IdentPath(name)) = e {
                    let args = parse_funcall_args(funcall_or_dot)?;
                    return Ok(ast::Exprs::Funcall(ast::FuncallExpr {
                        name,
                        args,
                    }));
                } else {
                    return Err(Box::new(make_custom_error("Expected callable object", atom_span)))
                }
            }
            rest => { panic!("Unexpected rule: {rest:?}"); }
        }
    }

    Ok(e)
}

fn parse_funcall_args(p: Pair<Rule>) -> ParseResult<Vec<ast::Expr>> {
    let inner = p.into_inner();
    let mut v = Vec::new();
    for expr in inner {
        v.push(parse_expr(expr)?);
    }
    return Ok(v);
}

fn parse_atom_expr(p: Pair<Rule>) -> ast::Exprs {
    fn wrap(e: ast::Atom) -> ast::Exprs { ast::Exprs::Atom(e) }
    // TODO
    let p = p.into_inner().next().expect("atoms should have exactly 1 inner pair");
    match p.as_rule() {
        Rule::number => {
            let a = parse_number(p);
            return wrap(ast::Atom::Number(a));
        },
        Rule::ident_path => {
            let a = parse_ident_path(p);
            return wrap(ast::Atom::IdentPath(a));
        }
        rest => todo!("parse_atom_expr: {rest:#?}"),
    }
}

fn parse_block(blk: Pair<Rule>) -> ParseResult<ast::BlockExpr> {
    let mut exprs = Vec::new();
    for expr in blk.into_inner() {
        exprs.push(parse_expr(expr)?);
    }
    Ok(ast::BlockExpr { exprs })
}

// Types //

fn parse_typeusage(_p: Pair<Rule>) -> ast::TypeUsage {
    ast::TypeUsage::Void
}

fn parse_typeusage_full(p: Pair<Rule>) -> ast::TypeUsageFull {
    ast::TypeUsageFull::void(p.as_span().into())
}

// Atoms //

fn parse_number(p: Pair<Rule>) -> ast::Number {
    util::warn_once!("WARN: Currently parsing all integers as i32");
    let num: i32 = p.as_str().parse().expect("Failed to parse i32 in parse_number");
    return ast::Number::I32(num);
}

fn parse_no_generic_ident_path(p: Pair<Rule>) -> ParseResult<ast::IdentPath> {
    let span = p.as_span();
    let path = parse_ident_path(p);
    if path.generic.is_some() {
        return Err(Box::new(make_custom_error("Unexpected Generic", span)));
    }

    Ok(path)
}

fn parse_ident_path(p: Pair<Rule>) -> ast::IdentPath {
    assert_eq!(p.as_rule(), Rule::ident_path);
    let s = p.as_span();
    let mut idents = vec![];
    let mut generic = None;
    for i in p.into_inner() {
        match i.as_rule() {
            Rule::ident => {
                idents.push(parse_ident(i));
            },
            Rule::op_ident_path_separator => (),
            Rule::generic_usage => {
                assert!(generic.is_none());
                generic = Some(parse_generic_usage(i));
            },
            r => panic!("unexpected rule {r:?}")
        }
    }
    ast::IdentPath { loc: Some(s.into()), path: idents, generic }
}

fn parse_ident(p: Pair<Rule>) -> ast::Ident {
    assert_eq!(p.as_rule(), Rule::ident);
    ast::Ident::new(p.as_str().to_string(), p.as_span().into())
}

// Generics //

fn parse_generic_usage(p: Pair<Rule>) -> ast::GenericUsage {
    assert_eq!(p.as_rule(), Rule::generic_usage);
    let s = p.as_span();
    let g = p.into_inner()
        .map(parse_ident_path)
        .collect();
    ast::GenericUsage {
        loc: s.into(),
        generics: g,
    }
}

// Util //

fn make_custom_error(txt: &str, span: pest::Span) -> PestError<Rule> {
    let ev = pest::error::ErrorVariant::CustomError { message: txt.to_string() };
    let e = PestError::new_from_span(ev, span);
    return e;
}


#[cfg(test)]
mod grammar_tests {
    use super::*;
    use pest::{parses_to, fails_with, consumes_to};

    #[test]
    fn ident_does_not_accept_keywords() {
        let cases = [ "nf", "match", "nt", "prove" ];
        for s in cases {
            fails_with! {
                parser: PestParser,
                input: s,
                rule: Rule::ident,
                positives: vec![Rule::ident],
                negatives: vec![],
                pos: 0
            }
        }
    }

    #[test]
    fn ident_does_accept_strings_that_contain_a_keyword() {
        let cases = [ "_nf", "somethingnf", "a_nf_b", "nfa", "nf_a" ];
        for s in cases {
            parses_to! {
                parser: PestParser,
                input: s,
                rule: Rule::ident,
                tokens: [ ident(0, s.len()) ]
            }
        }
    }

    #[test]
    fn strings() {
        let s = r#""asdf""#;
        parses_to! {
            parser: PestParser,
            input:  s,
            rule: Rule::string,
            tokens: [ string(0, s.len(), [
                 char_contents(1, 2),
                 char_contents(2, 3),
                 char_contents(3, 4),
                 char_contents(4, 5),
            ])]
        }

        let s = r#""a\"sd""#;
        parses_to! {
            parser: PestParser,
            input: s,
            rule: Rule::string,
            tokens: [ string(0, s.len(), [
                 char_contents(1, 2),
                 char_contents(2, 4),
                 char_contents(4, 5),
                 char_contents(5, 6),
            ])]
        }
        let s = r#""""#;
        parses_to! {
            parser: PestParser,
            input: s,
            rule: Rule::string,
            tokens: [ string(0, s.len(), [
            ])]
        }
        let s = r#""\"""#;
        parses_to! {
            parser: PestParser,
            input: s,
            rule: Rule::string,
            tokens: [ string(0, s.len(), [
                 char_contents(1, 3)
            ])]
        }
    }

    #[test]
    fn escapes() {

    }

    #[test]
    fn floats() {
        let cases = [ "0.0", ".0", ".1", "1.", "1.1", ".01230", "1213.", "12321.0213" , "123.0e-10" , "343e+450", "454e3"];
        for s in cases {
            let len = s.len();
            parses_to! {
                parser: PestParser,
                input: s,
                rule: Rule::number,
                tokens: [ number(0, len, [ decimal_number(0, len) ]) ]
            }
        }
    }

    #[test]
    fn decimal_integers() {
        let cases = [ "123", "53", "01", "59", "123", "0", "0000" ];
        for s in cases {
            let len = s.len();
            parses_to! {
                parser: PestParser,
                input: s,
                rule: Rule::number,
                tokens: [ number(0, len, [ decimal_integer(0, len) ]) ]
            }
        }
    }

    #[test]
    fn hex_number() {
        let cases = [ "0x1F23", "0x53", "0x01", "0x59", "0x123FA", "0x0", "0x0000" ];
        for s in cases {
            let len = s.len();
            parses_to! {
                parser: PestParser,
                input: s,
                rule: Rule::number,
                tokens: [ number(0, len, [ hex_number(0, len) ]) ]
            }
        }
    }

    #[test]
    fn bin_number() {
        let cases = [ "0b1100110", "0b101010", "0b01", "0b10", "0b11101", "0b0", "0b0000" ];
        for s in cases {
            let len = s.len();
            parses_to! {
                parser: PestParser,
                input: s,
                rule: Rule::number,
                tokens: [ number(0, len, [ bin_number(0, len) ]) ]
            }
        }
    }

    #[test]
    fn oct_number() {
        let cases = [ "0o123", "0o53", "0o01", "0o57", "0o123", "0o0", "0o0000" ];
        for s in cases {
            let len = s.len();
            parses_to! {
                parser: PestParser,
                input: s,
                rule: Rule::number,
                tokens: [ number(0, len, [ oct_number(0, len) ]) ]
            }
        }
    }
}
