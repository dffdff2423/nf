use std::fmt::{Debug, Display};
use std::fmt::Write;

use crate::ast;

#[derive(Clone, Copy, Hash, PartialEq, Eq)]
pub struct SrcLocation {
    line: usize,
    col: usize,
    idx: usize,
}

impl Debug for SrcLocation {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "({}, {}) @ {}", self.line, self.col, self.idx)
    }
}

impl Display for SrcLocation {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "{}:{}", self.line, self.col)
    }
}

#[allow(unused)]
#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq)]
pub struct SrcSpan {
    pub start: SrcLocation,
    pub end: SrcLocation,
}

impl SrcSpan {
    pub fn format_error(&self, m: &ast::File, txt: &str) -> CompileError {
        let mut etxt = String::new();
        writeln!(etxt, "{}:{}:", m.path.unwrap_or(""), self.start).expect("failed to format error");
        etxt.push_str(txt);

        return CompileError { txt: etxt };

        // TODO: fancy errors
        //let mut iter = m.txt.lines().skip(self.start.line - 2).take(self.end.line-self.start.line + 1);
        //let line_num_dig = digits(self.end.line as f64);
        //write!(etxt, "{:>line_num_dig$}")
    }
}

impl From<pest::Span<'_>> for SrcSpan {
    fn from(s: pest::Span) -> Self {
        let start = s.start_pos();
        let (line, col) = start.line_col();
        let start = SrcLocation { line, col, idx: start.pos() };

        let end = s.end_pos();
        let (line, col) = end.line_col();
        let end = SrcLocation { line, col, idx: end.pos() };

        return SrcSpan { start, end };
    }
}

#[derive(Debug)]
pub struct CompileError {
    txt: String,
}

// TODO: fancy errors
//pub(crate) fn digits<T: Into<f64>>(n: T) -> usize {
//    let n = f64::abs(n.into());
//    if n == 0.0 { return 1 }
//    f64::ceil(f64::log10(n)) as usize
//}

impl Display for CompileError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(&self.txt)
    }
}

impl std::error::Error for CompileError {}

/// Print the text inside once
macro_rules! warn_once {
    ($($arg:tt)*) => {{
        use ::std::sync::Once;
        static WARNING: Once = Once::new();
        WARNING.call_once(|| {
            eprintln!($($arg)*)
        });
    }}
}
pub(crate) use warn_once;
