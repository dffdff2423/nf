To generate nfc tests run: cargo xtask generate-tests

Tests that end with the .nocmp.nf are expected to not compile.
It is an error if they do.
