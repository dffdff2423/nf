glbl nf main() {
	// right now we assume all functions take an i32
	print_hello(17);
};


nf print_hello() {
	__intrinsic::dumpchar(72);
	__intrinsic::dumpchar(101);
	__intrinsic::dumpchar(108);
	__intrinsic::dumpchar(108);
	__intrinsic::dumpchar(111);

	__intrinsic::dumpchar(32);

	__intrinsic::dumpchar(87);
	__intrinsic::dumpchar(111);
	__intrinsic::dumpchar(114);
	__intrinsic::dumpchar(108);
	__intrinsic::dumpchar(100);
	__intrinsic::dumpchar(33);

	__intrinsic::dumpchar(10);
};
