use std::process::{Command, Stdio};

use expect_test::expect_file;
use expect_test::ExpectFile;

fn case(path: &str, out: ExpectFile) {
    let obj_path = format!("{DUMP_DIR}/{path}.o");
    let nfc = Command::new(NFC_PATH)
        .arg(format!("{TEST_DIR}/input/{path}"))
        .arg("-o")
        .arg(&obj_path)
        .stdout(Stdio::piped())
        .stderr(Stdio::piped())
        .spawn()
        .expect("failed to run nfc")
        .wait_with_output()
        .expect("failed to wait for nfc");
    let stdout = String::from_utf8_lossy(&nfc.stdout);
    if !nfc.status.success() {
        let code = nfc.status.code().expect("failed to get exit code");
        let stderr = String::from_utf8_lossy(&nfc.stderr);
        let exout = format!("code:\n{code}\nstdout:\n{stdout}\nstderr:\n{stderr}");
        out.assert_eq(&exout);
        return;
    }

    let exe_path = format!("{DUMP_DIR}/{path}.bin");
    let ld = Command::new("ld")
        .arg(&obj_path)
        .arg(format!("{RT_DIR}/start.o"))
        .arg(format!("{RT_DIR}/intrinsics.o"))
        .arg("-o")
        .arg(&exe_path)
        .spawn()
        .expect("failed to run as")
        .wait()
        .expect("failed to wait for ld");
    assert!(ld.success(), "ld failed, make shure to run ./rt/buildobj.sh");

    let proc = Command::new(exe_path)
        .stdout(Stdio::piped())
        .stderr(Stdio::piped())
        .spawn()
        .expect("failed to run compiled proccess")
        .wait_with_output()
        .expect("failed to wait for compiled proccess");

    let stdout = String::from_utf8_lossy(&proc.stdout);
    let code = proc.status.code().expect("failed to get exit code");
    let stderr = String::from_utf8_lossy(&proc.stderr);
    let exout = format!("code:\n{code}\nstdout:\n{stdout}\nstderr:\n{stderr}");
    out.assert_eq(&exout);
}
